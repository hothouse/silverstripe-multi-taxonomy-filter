<?php

/**
 * Created by PhpStorm.
 * User: ed
 * Date: 22/06/17
 * Time: 10:44 AM
 */
class MultiTaxonomyMasonryPage extends Page {

	private static $db = array(
		'RegularChildrenMenuMode' => 'Boolean',
		'HideFilter' => 'Boolean'
	);

	/**
	 * optional filter for landing on the page - i.e. having multiple instances of this page to show different filter states
	 * if a parent taxonomy is left "empty" (no terms selected) it'll show all
	 *
	 * @var array
	 */
	private static $many_many = array(
		'TaxonomyTerms' => 'MultiTaxonomyTerm'
	);

	public static $TaxChildrenCache = null;

	public function getCMSFields() {
		$fields = parent::getCMSFields(); // TODO: Change the autogenerated stub

		$parentTags = MultiTaxonomyTerm::get()->filter([
			'ParentID' => 0,
			'External' => false
		]);
		foreach ($parentTags as $parentTag) {
			$fields->addFieldToTab(
				'Root.PreselectedTaxonomy',
					ListboxField::create('LandingTaxonomyFilters_' . $parentTag->ID, $parentTag->Name . ' Tags',
						MultiTaxonomyTerm::get()->filter([
							'ParentID' => $parentTag->ID
						])->sort('Name')->map('ID', 'Name')->toArray()
					)
					->setMultiple(true)->setValue($parentTag->PreFilteredForPage($this->ID)));
		}

		$fields->addFieldToTab('Root.PreselectedTaxonomy', CheckboxField::create('RegularChildrenMenuMode'));
		$fields->addFieldToTab('Root.PreselectedTaxonomy', CheckboxField::create('HideFilter'));

		return $fields;
	}

	public function onAfterWrite() {
		parent::onAfterWrite();
		$controller = Controller::curr();
		$request = $controller->getRequest();
		if (strpos($request->getVar('url'), 'pages/edit/EditForm') && $request->postVar('Action') == 'content') {
			// funky re-group of taxonomy tags - need to grab all the chosen fields and attach the values to the to be saved object
			$TaxonomyItems = array();
			$this->TaxonomyTerms()->removeAll();

			foreach ($request->postVars() as $key => $item) {
				if (substr($key, 0, strlen('LandingTaxonomyFilters_')) == 'LandingTaxonomyFilters_') {
					$TaxonomyItems = array_merge($TaxonomyItems, $item);
				}
			}
			foreach ($TaxonomyItems as $tagID) {
				$tagID = (int) $tagID;
				if ($tagID < 1) continue;
				$this->TaxonomyTerms()->add($tagID);
			}

		}
	}

	/**
	 * return Children for all pages of type MultiTaxonomyMasonryPage, optionally filtered by allowed_children
	 *
	 * @return DataList
	 */
	public function TaxChildren() {
		if (!empty(self::$TaxChildrenCache)) return self::$TaxChildrenCache;
		$childrenClasses = $this->stat('allowed_children');

		$myInstances = MultiTaxonomyMasonryPage::get()->map('ID', 'ID')->toArray();

		$where = array();
		$where[] = sprintf('"ParentID" IN (%s)', implode(', ', $myInstances));
		if (count($childrenClasses)) {
			// grab all Children of all pages of the allowed types of this class
			$where[] = sprintf('"ClassName" IN (%s)', implode(', ', array_map(
				function ($el) {
					return "'$el'";
				}, $childrenClasses)));
		}

		//$sql = SQLSelect::create('ID', 'SiteTree', $where)->sql();
		$result = SQLSelect::create('ID', 'SiteTree', $where)->execute();
		$ids = array();
		foreach($result as $row) {
			$ids[] = $row['ID'];
		}
		if (count($ids)) {
			$return = SiteTree::get()->where(array(sprintf('ID IN (%s)', implode(', ', $ids))));
		} else {
			$return = ArrayList::create();
		}
		self::$TaxChildrenCache = $return;
		return $return;
	}

	/**
	 * return TaxChildren filtered by TaxonomyTerms, used for menu etc
	 *
	 * @return DataList|ArrayList
	 */
	public function Children() {
		if (!$this->TaxonomyTerms()->count() || $this->RegularChildrenMenuMode) {
			return parent::Children();
		}

		$children = $this->TaxChildren();

		$return = array();
		// for each of this pages TaxonomyTerms filter Children by
		foreach ($this->TaxonomyTerms() as $term) {
			foreach ($children as $child) {
				//if ($child->TaxonomyTerms())
				if ($child->TaxonomyTerms()->filter('ID', $term->ID)->count()) {
					$return[] = $child;
				}
			}
		}
		return ArrayList::create($return);
	}
}

class MultiTaxonomyMasonryPage_Controller extends Page_Controller {

	public function init() {
		parent::init();
		Requirements::css(MODULE_MULTITAXONOMYFILTER_PATH . '/templates/css/multi-taxonomy.css');
		Requirements::javascript(MODULE_MULTITAXONOMYFILTER_PATH . '/javascript/multitoggle.js');
		Requirements::javascript('https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js');
		Requirements::javascript('https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js');
	}

	/**
	 * return (used) parent taxonomies
	 *
	 * @return DataList|ArrayList
	 */
	public function Taxonomies($used = true) {
		if (!$used) {
			return MultiTaxonomyTerm::get()->filter('ParentID', 0);
		}

		$childrenClasses = $this->data()->stat('allowed_children');
		$termClassName = 'MultiTaxonomyTerm';
		$usedTaxonomyIds = array();
		foreach ($childrenClasses as $class) {
			$many_manyName = array_search($termClassName, $class::create()->stat('many_many'));
			$connectorTable = $class . '_' . $many_manyName;
			$query = SQLQuery::create($termClassName . 'ID', $connectorTable)->execute();
			foreach ($query as $row) {
				$id = (int) reset($row);
				$usedTaxonomyIds[$id] = $id;
			}
		}

		$parents = [];
		foreach (MultiTaxonomyTerm::get()->filter('ParentID', 0) as $parent) {
			$childIDs = MultiTaxonomyTerm::get()->filter('ParentID', $parent->ID)->map('ID', 'ID')->toArray();
			if (array_intersect($usedTaxonomyIds, $childIDs)) {
				$parents[] = $parent;
			}
		}
		return ArrayList::create($parents);
	}
}