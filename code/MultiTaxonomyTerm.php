<?php

/**
 * Represents a single taxonomy term. Can be re-ordered in the CMS, and the default sorting is to use the order as
 * specified in the CMS.
 *
 * @method MultiTaxonomyTerm Parent()
 * @package taxonomy
 */
class MultiTaxonomyTerm extends DataObject
{
    private static $db = array(
        'Name' => 'Varchar(255)',
        'Sort' => 'Int',
	    'FilterType' => 'Int',
	    'Identifier' => 'Varchar(30)',
	    'ShowAllWhenUnselected' => 'Boolean',
	    'FilterOffByDefault' => 'Boolean',
	    'LogicalAndMode' => 'Boolean',
	    'HideInFilter' => 'Boolean',
	    'External' => 'Boolean'
    );

    private static $has_many = array(
        'Children' => 'MultiTaxonomyTerm'
    );

    private static $has_one = array(
        'Parent' => 'MultiTaxonomyTerm',
	    'Image' => 'Image'
    );

    private static $belongs_many_many = array(
    	'LandingFiltered' => 'MultiTaxonomyMasonryPage'
    );

    private static $extensions = array(
        'Hierarchy'
    );

    private static $casting = array(
        'TaxonomyName' => 'Text'
    );

    private static $default_sort = 'Sort';

    public static function ParentTaxonomies() {
        return MultiTaxonomyTerm::get()->filter('ParentID', 0);
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        // For now moving taxonomy terms is not supported.
        $fields->removeByName('ParentID');
        $fields->removeByName('Sort');

        if (!$this->ID || $this->ParentID > 0) {
        	$fields->removeByName('FilterType');
        	//$fields->removeByName('Identifier');
        	$fields->removeByName('HideInFilter');
        } else {
        	$fields->dataFieldByName('FilterType')->setRightTitle('Type of filter to be used, please don\'t change');
        	$fields->dataFieldByName('Identifier')->setRightTitle('Filter identifier, please don\'t change, used in page logic');
        }

        $childrenGrid = $fields->dataFieldByName('Children');
        if ($childrenGrid) {
            $deleteAction = $childrenGrid->getConfig()->getComponentByType('GridFieldDeleteAction');
            $addExistingAutocompleter = $childrenGrid->getConfig()->getComponentByType('GridFieldAddExistingAutocompleter');

            $childrenGrid->getConfig()->removeComponent($addExistingAutocompleter);
            $childrenGrid->getConfig()->removeComponent($deleteAction);
            $childrenGrid->getConfig()->addComponent(new GridFieldDeleteAction(false));

            // Setup sorting of TaxonomyTerm siblings, and fall back to a manual NumericField if no sorting is possible
            if (class_exists('GridFieldOrderableRows')) {
                $childrenGrid->getConfig()->addComponent(new GridFieldOrderableRows('Sort'));
            } elseif (class_exists('GridFieldSortableRows')) {
                $childrenGrid->getConfig()->addComponent(new GridFieldSortableRows('Sort'));
            } else {
                $fields->addFieldToTab('Root.Main', NumericField::create('Sort', 'Sort Order')
                                                                ->setDescription('Enter a whole number to sort this term among siblings (0 is first in the list)')
                );
            }
        }

        $fields->dataFieldByName('LogicalAndMode')->setDescription('Only show items that have all features selected in the filter (as opposed to showing all that have at least one).');

        if ($this->ID) {
        	// options only for single tick mode parents
	        if ($this->ParentID) {
		        $fields->removeByName('ShowAllWhenUnselected');
		        $fields->removeByName('FilterOffByDefault');
		        $fields->removeByName('LogicalAndMode');
	        } else {
		        if ($this->Children()->count() > 1) {
			        $fields->removeByName('ShowAllWhenUnselected');
//			        $fields->removeByName('FilterOffByDefault');
		        } else {
			        $fields->removeByName('LogicalAndMode');
		        }
	        }
        } else {
        	// don't show if not saved yet or for not parents
	        $fields->removeByName('ShowAllWhenUnselected');
	        $fields->removeByName('FilterOffByDefault');

        }
        return $fields;
    }

    /**
     * Get the top-level ancestor which doubles as the taxonomy.
     *
     * @return MultiTaxonomyTerm
     */
    public function getTaxonomy()
    {
        return ($parent = $this->Parent()) && $parent->exists()
            ? $parent->getTaxonomy()
            : $this;
    }

    /**
     * Gets the name of the top-level ancestor
     *
     * @return string
     */
    public function getTaxonomyName()
    {
        return $this->getTaxonomy()->Name;
    }

    public function onBeforeDelete()
    {
        parent::onBeforeDelete();

        foreach ($this->Children() as $term) {
            $term->delete();
        }
    }

    public function PreFilteredForPage($pageID) {
	    $return = array();
	    foreach($this->Children() as $child) {
		    foreach ($child->LandingFiltered() as $page) {
			    if ($page->ID == $pageID) $return[] = $child->ID;
		    }
	    }
	    return $return;
    }

    public function CssName() {
    	return URLSegmentFilter::create()->filter($this->Name . '_' . $this->ParentID);
    }


    public function CssPreDeactived() {
	    $className = 'in-active';
    	$parent = $this->Parent();

    	//special case on/off (single taxonomy) controls
	    // if landing state of page selects this control, it's on, otherwise it depends on FilterOffByDefault state
    	if ($parent->Children()->count() == 1 || $parent->LogicalAndMode) {
		    $page = Controller::curr()->data();
		    $pageIDs = $this->LandingFiltered()->map('ID', 'ID')->toArray();
		    if (array_key_exists($page->ID, $pageIDs))  {
			    // active if explicitly set as landing filter
			    return '';
		    } else {
			    if ($parent->FilterOffByDefault) {
				    return $className;
			    } else {
			    	return '';
			    }
		    }
	    }

	    $return = $this->PreActive() ? '' : $className;
	    return $return;
    }

	/**
	 * called in template, used for pre selecting filter based on "LandingFiltered" values, returns true if:
	 * - no child selected for this parent
	 * - this child is selected
	 *
	 * @return string
	 */
    public function PreActive() {
	    $page = Controller::curr()->data();

	    if (!$page instanceof MultiTaxonomyMasonryPage) {
	        // for filter display on other pages than MultiTaxonomyMasonryPage
	        return true;
        }

	    if ($page && $page->ID) {
		    $pageIDs = $this->LandingFiltered()->map('ID', 'ID')->toArray();
		    if (array_key_exists($page->ID, $pageIDs)) return true;

		    $parent = $this->Parent();
		    if ($parent && $parent->ID) {
			    $ParentTaxonomyChildren = $parent->Children()->map('ID')->toArray();
			    $pageFilterSelection = $page->TaxonomyTerms()->map('ID')->toArray();
			    if (!array_intersect($ParentTaxonomyChildren, $pageFilterSelection)) {
				    return true;
			    }
		    }
	    }
	    return false;
    }

    public function InclusiveSingleFilter() {
    	return $this->ShowAllWhenUnselected ? '1' : '0';
    }

    public function LogicalAndMultiFilter() {
    	return $this->LogicalAndMode ? '1' : '0';
    }

    public function OffByDefaultFilter() {
    	return $this->FilterOffByDefault ? '1' : '0';
    }

    public function generateIdentifier() {
    	if ($this->Identifier) return;
	    $filter = URLSegmentFilter::create();
	    $identifier = strtoupper($filter->filter(substr($this->Name, 0, 20)));

	    $counter = 1;
	    $origIdentifier = $identifier;
	    while ($obj = self::get()->filter('Identifier', $identifier)->first()) {
		    $identifier = $origIdentifier . $counter;
		    $counter++;
	    }

	    $this->Identifier = $identifier;
    }
}
