<?php
/**
 * Created by PhpStorm.
 * User: hothouse
 * Date: 24/10/18
 * Time: 11:12 AM
 */
namespace HotHouse\MultiTaxonomy;
use PageController;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DB;
use SilverStripe\ORM\Queries\SQLSelect;
use SilverStripe\View\Requirements;

class MultiTaxonomyMasonryPageController extends PageController {

    public function init() {
        parent::init();

        Requirements::javascript('https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js');
        Requirements::javascript('https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js');

        Requirements::css('hothouse/silverstripe-multi-taxonomy-filter:templates/css/multi-taxonomy.css');
        Requirements::javascript('hothouse/silverstripe-multi-taxonomy-filter:javascript/multitoggle.js');
    }

    /**
     * return (used) parent taxonomies
     *
     * @return DataList|ArrayList
     */
    public function Taxonomies($used = true) {
        if (!$used) {
            return MultiTaxonomyTerm::get()->filter('ParentID', 0);
        }

        $object = MultiTaxonomyMasonryPage::get()->first();
        $childrenClasses = $object->config()->get('allowed_children');

        $termClassName = 'Hothouse\MultiTaxonomy\MultiTaxonomyTerm';
        $schema = static::getSchema();
        $termTableName = $schema->tableName($termClassName);
        $usedTaxonomyIds = array();
        foreach ($childrenClasses as $class) {
            $many_manyName = array_search($termClassName, $class::create()->config()->get('many_many'));
            if (!$many_manyName) continue;
            $table = $schema->tableName($class);
            $connectorTable = $table . '_' . $many_manyName;
            //see if table exists
            $exists = DB::query('SELECT "table_name" FROM "information_schema"."tables" WHERE "table_name" = \''.$connectorTable.'\';')->value();
            if ($exists) {
                $query = SQLSelect::create($termTableName . 'ID', $connectorTable)->execute();
            } else {
                continue;
            }
            foreach ($query as $row) {
                $id = (int) reset($row);
                $usedTaxonomyIds[$id] = $id;
            }
        }

        $parents = [];
        foreach (MultiTaxonomyTerm::get()->filter('ParentID', 0) as $parent) {
            $childIDs = MultiTaxonomyTerm::get()->filter('ParentID', $parent->ID)->map('ID', 'ID')->toArray();
            if (array_intersect($usedTaxonomyIds, $childIDs)) {
                $parents[] = $parent;
            }
        }
        return ArrayList::create($parents);
    }
}