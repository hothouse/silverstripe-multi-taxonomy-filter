/**
 * Created by ed on 22/06/17.
 */
$(document).ready(function() {

    function Gridfilter() {
        if (!(this instanceof Gridfilter)) {
            // the constructor was called without "new".
            return new Gridfilter();
        }

        var me = this;

        this.doMultiTaxFilter = function () {
            var grid = [];
            var filterString;
            var filterGroups;

            window.setTimeout(function(){
                filterGroups = $('.toggle-button');
                grid = $('.grid .element-item');

                filterGroups.each(function(i, obj) {
                    var logical_and_mode = $(obj).data('logical-and');
                    var off_by_default = $(obj).data('off-by-default');
                    var orFilter = [];
                    $(obj).find('label.active').each(function(j, innerObj) {
                        orFilter.push('.' + $(innerObj).data('filter'));
                    });

                    if (orFilter.length) {
                        if (logical_and_mode) {
                            filterString = orFilter.join('');
                        } else {
                            filterString = orFilter.join(',');
                        }
                        grid = grid.filter(filterString);
                    } else {
                        // if only one item in group, don't select back on
                        if ($(obj).children().length > 1) {
                            if (!off_by_default) {
                                // all unselected in group, select them all back on
                                $(obj).find('input').trigger( "click");
                            }
                        } else {
                            // if single taxonomy & !ShowAllWhenUnselected for this filter group -> show just the non tagged
                            if ($(obj).data('exclusive') != '1') {
                                grid = grid.filter(':not(.' + $(obj).children().first().data('filter') + ')');
                            }
                        }
                    }
                });

                if (me.externalControls) {
                    me.externalControls.each(function(obj) {
                        grid = $(grid).filter(me.externalCallback(grid, me.externalControls));
                    });
                }

                me.pushFilterToGrid(grid);

            }, 500);
        };
        this.pushFilterToGrid = function (grid) {
            filterString = '';
            if (grid.length > 0) {
                grid.each (function(i, element) {
                    filterString += '#' + $(element).prop('id') + ',';
                });
                filterString = filterString.slice(0, -1);
            } else {
                filterString = '.NONE';
            }

            me.$grid.isotope({ filter: filterString });
        };
        this.$grid = $('.grid').isotope({
            itemSelector: '.element-item',
            layoutMode: 'masonry'
        });

        this.$grid.imagesLoaded().progress( function() {
            me.$grid.isotope('layout');
        });
    }

    var gridfilter = new Gridfilter();

    $(document).on('change', '.toggle-button input.multi-filter', function(){
        if ($(this).parent().find('i.fa').hasClass('fa-check-square-o')) {
            $(this).parent().find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
        } else {
            $(this).parent().find('i.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
        }
        gridfilter.doMultiTaxFilter();
    });

    $(document).find('.in-active').trigger('click');

    $(document).trigger('taxFilterReady', [gridfilter]);

// external js: isotope.pkgd.js

});