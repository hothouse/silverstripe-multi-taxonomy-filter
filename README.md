Multi Taxonomy Filter
===============

**Author:** Eduard Briem

Installation
------------
1. add many_many connection on the Dataobject/PageType to MultiTaxonomyTerm ()

```php
	private static $many_many = array(
		'TaxonomyTerms' => 'MultiTaxonomyTerm'
	);
```

2. create a Dataextension for MultiTaxonomyTerm to connect Taxonomy back to the DataObject you want taxonomy for
   (many_many connection) and attach inside yml
   
```php
class MultiTaxonomyAttachDataExtension extends DataExtension {

	private static $belongs_many_many = array(
		'MyDataObjects' => 'MyDataObject'
	);


	public function ChildrenForObject($myDataobject) {
		$return = array();
		foreach($this->owner->Children() as $child) {
			foreach ($child->MyDataObjects() as $myDataobject) {
				if ($myDataobject->ID == $myDataobjectID) $return[] = $child->ID;
			}
		}
		return $return;
	}
}
```

add to your MyDataObject

```php
	public function onAfterWrite() {
		parent::onAfterWrite();
		// funky re-group of taxonomy tags - need to grab all the chosen fields and attach the values to the to be saved object
		$controller = Controller::curr();
		$request = $controller->getRequest();
		$TaxonomyItems = array();

		foreach ($request->postVars() as $key => $item) {
			if (substr($key, 0, strlen('MultiTags_')) == 'MultiTags_') {
				$TaxonomyItems = array_merge($TaxonomyItems, $item);
			}
		}

		if (count($TaxonomyItems)) {
			$this->TaxonomyTerms()->removeAll();
		}
		
		foreach ($TaxonomyItems as $tagID) {
			$tagID = (int) $tagID;
			if ($tagID < 1) continue;
			$this->TaxonomyTerms()->add($tagID);
		}
	}

```
   
```yml
MultiTaxonomyTerm:
  extensions:
    - MultiTaxonomyAttachDataExtension
```

3. attach MultiTaxonomyTermExtension to the DataObject for admin fields

```yml
MyDataObject:
  extensions:
    - MultiTaxonomyTermExtension
```


4. MultiTaxonomyMasonryPage is to display a filtering taxonomy. 
   It can be used multiple times in the site, each instance can have it's own pre-filter set. 
   Each instance displays all Children from all instances of MultiTaxonomyMasonryPage, optionally filtered by allowed_children (see below).

4a. When using the MultiTaxonomyMasonryPage, allow Children (also used to filter out unused taxonomy parents in masonry):

```yml
MultiTaxonomyMasonryPage:
  allowed_children:
    - MyDataObject
```