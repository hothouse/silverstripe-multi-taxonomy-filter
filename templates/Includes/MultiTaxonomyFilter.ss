<div class="row <% if $HideFilter %>hidden<% end_if %>">
    <% loop $Taxonomies %>
        <div class="mt-filter">
            <label>$Name</label>
            <br>
            <div class="toggle-button btn-group" data-name="$CssName" data-toggle="buttons" data-exclusive="$InclusiveSingleFilter" data-logical-and="$LogicalAndMultiFilter" data-off-by-default="$OffByDefaultFilter">
                <% loop $Children %>
                   <% include ToggleButton %>
                <% end_loop %>
            </div>
        </div>
    <% end_loop %>
</div>